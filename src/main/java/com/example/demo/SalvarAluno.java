package com.example.demo;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Modal.Aluno;
import com.example.demo.Modal.AlunoRepository;

@RestController
@RequestMapping("/salvar/aluno")
public class SalvarAluno {
	@Autowired
	private AlunoRepository pr;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Aluno> novoAluno(@RequestBody Aluno a){
		Aluno pSalva=pr.save(a);
		URI uri=ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(pSalva.getId()).toUri();
	return ResponseEntity.created(uri).body(pSalva);
	}
	
}
