package com.example.demo;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.Modal.Aluno;
import com.example.demo.Modal.AlunoRepository;


@RestController
@RequestMapping("/atualizar/aluno")
public class AtualizarAluno {
	
	@Autowired
	private AlunoRepository pr;
	
@PutMapping("/{id}")
public ResponseEntity<Aluno> atualizar(@PathVariable int id, @Valid @RequestBody Aluno a){
	Optional <Aluno> pSalva= pr.findById(id);
	BeanUtils.copyProperties(a, pSalva.get(), "id");
	pr.save(pSalva.get());
	return ResponseEntity.ok(pSalva.get());


}
	
	
}
