package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Modal.Aluno;
import com.example.demo.Modal.AlunoRepository;


@RestController
@RequestMapping("/Get/Aluno")
public class GetAluno {


	
	@Autowired
	private AlunoRepository pr;
	
	@GetMapping
	public ResponseEntity<?> listar() 
	{
		List<Aluno> aluno= pr.findAll();
		if (aluno.isEmpty()){
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(aluno);
	};
	@GetMapping("/{id}")
	public Aluno listaAluno(@PathVariable("id")int id ){
		
		return pr.findById(id).get();
	}

}
